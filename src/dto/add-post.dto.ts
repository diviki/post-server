import { IsNotEmpty, Min, Contains } from 'class-validator';

export class AddPostDto {
  @IsNotEmpty() description: string;
}
