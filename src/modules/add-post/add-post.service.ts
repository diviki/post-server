import { AddPost } from './../../models/add-post.entity';
import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AddPostService {
  constructor(
    @InjectRepository(AddPost)
    private readonly addPostRepo: Repository<AddPost>,
  ) {}

  async postList(params, res): Promise<any> {
    try {
      let skip = 0;
      let take = 0;
      let order = params.order || 'DESC';
      let sortby = params.sortby || 'id';
      let sort = {};
      sort[sortby] = order;
      if (!isNaN(params.skip) && !isNaN(params.take)) {
        skip = Number(params.skip);
        take = Number(params.take);
      }
      let filter = `id IS NOT NULL`;
      if (params.id) {
        filter += ` AND id = '${params.id}'`;
      }
      if (params.status) {
        filter += ` AND status = '${params.status}'`;
      }
      if (params.description) {
        filter += ` AND post_description ILIKE '%${params.description}%'`;
      }

      const total = await this.addPostRepo.count({ where: filter });
      const data = await this.addPostRepo.find({
        where: filter,
        skip: skip,
        take: take,
        order: sort,
      });
      return res.status(200).send({ status: HttpStatus.OK, total, data });
    } catch (error) {
      return res.status(400).send({
        message: 'Something Went Wrong',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  async addPost(description, file, filename, filePath, res): Promise<any> {
    try {
      const { originalname, mimetype } = file;
      const postData = new AddPost();
      postData.post_description = description;
      postData.image_name = originalname;
      postData.mime_type = mimetype;
      postData.file_path = filePath;
      postData.file_name = filename;
      const data = await this.addPostRepo.save(postData);

      return res
        .status(200)
        .send({ message: 'Post List', status: HttpStatus.OK, data });
    } catch (error) {
      return res.status(400).send({
        message: 'Something Went Wrong',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  async likeCountForPost(id, res, params): Promise<any> {
    try {
      let count: number = 0;
      const { type } = params;
      const checkPostExists = await this.addPostRepo.findOne({ id });
      if (!checkPostExists)
        return res.status(400).send({
          message: 'No Post Exists ..!!',
          status: HttpStatus.BAD_REQUEST,
        });

      if (type === 'add') {
        count = checkPostExists.like_count + 1;
      } else if (type === 'remove') {
        count = checkPostExists.like_count - 1;
      } else {
        return res.status(400).send({
          message: 'No Actions Allowed ..!!',
          status: HttpStatus.BAD_REQUEST,
        });
      }
      await this.addPostRepo.update({ id }, { like_count: count });
      return res
        .status(200)
        .send({ message: 'Post Liked', status: HttpStatus.OK, count });
    } catch (error) {
      return res.status(400).send({
        message: 'Something Went Wrong',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }
}
