import { AddPostService } from './add-post.service';
import {
  Controller,
  Get,
  Query,
  Res,
  Post,
  Body,
  UseInterceptors,
  UploadedFiles,
  HttpStatus,
  Put,
  Param,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { AddPostDto } from '../../dto/add-post.dto';
const fs = require('fs');

@Controller('add-post')
export class AddPostController {
  constructor(private readonly postService: AddPostService) {}

  @Get()
  postList(@Query() param, @Res() res): Promise<any> {
    return this.postService.postList(param, res);
  }

  @Post()
  @UseInterceptors(FilesInterceptor('image'))
  async addPost(
    @Body() data: AddPostDto,
    @UploadedFiles() file,
    @Res() res,
  ): Promise<any> {
    try {
      const { description } = data;
      if (!fs.existsSync('src/uploads')) {
        fs.mkdirSync('src/uploads');
      }
      if (!file.length)
        return res.status(400).send({
          message: 'Something Went Wrong',
          status: HttpStatus.BAD_REQUEST,
        });

      let filename = file[0].originalname;
      const index = filename.lastIndexOf('.');
      filename = `${filename.slice(0, index)}_${Date.now()}${filename.slice(
        index,
      )}`;
      const filePath = `src/uploads/${filename}`;
      await fs.writeFileSync(filePath, file[0].buffer);
      return this.postService.addPost(
        description,
        file[0],
        filename,
        filePath,
        res,
      );
    } catch (error) {
      console.log(error);
      return res.status(400).send({
        message: 'Something Went Wrong',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @Put('/like/:id')
  likeCountForPost(@Param('id') id, @Res() res, @Query() params): Promise<any> {
    if (!params.type)
      return res.status(400).send({
        message: 'Cannot Do Anything ..!!',
        status: HttpStatus.BAD_REQUEST,
      });
    return this.postService.likeCountForPost(Number(id), res, params);
  }
}
