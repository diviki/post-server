import { Test, TestingModule } from '@nestjs/testing';
import { AddPostController } from './add-post.controller';

describe('AddPost Controller', () => {
  let controller: AddPostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AddPostController],
    }).compile();

    controller = module.get<AddPostController>(AddPostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
