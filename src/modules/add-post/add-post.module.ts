import { AddPost } from './../../models/add-post.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { AddPostController } from './add-post.controller';
import { AddPostService } from './add-post.service';

@Module({
  imports: [TypeOrmModule.forFeature([AddPost])],
  controllers: [AddPostController],
  providers: [AddPostService],
})
export class AddPostModule {}
