import { Test, TestingModule } from '@nestjs/testing';
import { AddPostService } from './add-post.service';

describe('AddPostService', () => {
  let service: AddPostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AddPostService],
    }).compile();

    service = module.get<AddPostService>(AddPostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
