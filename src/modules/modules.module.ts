import { Module } from '@nestjs/common';
import { AddPostModule } from './add-post/add-post.module';

@Module({
  imports: [AddPostModule]
})
export class ModulesModule {}
