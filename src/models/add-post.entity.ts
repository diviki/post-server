import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('AddPost')
export class AddPost {
  @PrimaryGeneratedColumn() id: number;
  @Column('text') post_description: string;
  @Column() image_name: string;
  @Column() mime_type: string;
  @Column() file_path: string;
  @Column() file_name: string;
  @Column({ default: null, nullable: true }) like_count: number;
  @Column({ default: true }) status: boolean;
  @Column({ default: false }) default_user_like: boolean;
  @CreateDateColumn() created_at: Date;
  @UpdateDateColumn() updated_at: Date;
}
