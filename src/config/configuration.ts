export default () => ({
  port: parseInt(process.env.PORT, 10) || 3400,
  database: {
    type: process.env.DATABASE_TYPE,
    host: process.env.DATABASE_HOST,
    name: process.env.DATABASE_NAME,
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
    password: process.env.DATABASE_PASSWORD,
    username: process.env.DATABASE_USERNAME,
  },
});
